const http = require("http");
const fs = require("fs");
const path = require("path");
const uuid = require("uuid");
const { STATUS_CODES } = require("http");

const htmlIndex = path.join(__dirname, "/page.html");
const jsonData = path.join(__dirname, "/jsonString.json");

const PORT = process.env.PORT || 7777;

const server = http.createServer((req, res) => {
  if (req.url === "/html") {
    fs.readFile(htmlIndex, "utf8", (err, data) => {
      if (err) {
        throw new Error(err);
      } else {
        res.write(data);
        res.end();
      }
    });
  } else if (req.url === "/json") {
    fs.readFile(jsonData, "utf8", (err, data) => {
      if (err) {
        throw new Error(err);
      } else {
        res.write(data);
        res.end();
      }
    });
  } else if (req.url === "/uuid") {
    const id = uuid.v4();
    res.write(`{ uuid: "${id}" }`);
    res.end();
  } else if (req.url.startsWith("/status")) {
    const code = req.url.split("/").pop();
    res.write(` ${code} : ${STATUS_CODES[code]}`);
    res.end();
  } else if (req.url.includes("/delay")) {
    const time = req.url.split("/").pop() * 1000;
    setTimeout(() => {
      res.write(STATUS_CODES[200]);
      res.end();
    }, time);
  } else {
    res.write(`404 : ${STATUS_CODES[404]}`);
    res.end();
  }
});

server.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}`);
});
